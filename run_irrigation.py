import sqlite3
from flask import Flask, render_template, request, redirect, url_for, flash
import smtplib
import time
import datetime
import json
from w1thermsensor import W1ThermSensor
from flask_wtf import FlaskForm
from wtforms import Form, validators, StringField, SubmitField, IntegerField, HiddenField
from wtforms.validators import NumberRange, InputRequired
import threading
# import schedule
import itertools
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
import board
import busio
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import RPi.GPIO as GPIO
import atexit
import os
import settings

app = Flask(__name__)
app.secret_key = os.getenv('SECRET_KEY')

GPIO.setwarnings(False)
i2c = busio.I2C(board.SCL, board.SDA)
ds18b20 = W1ThermSensor()
ads = ADS.ADS1015(i2c)
ads.gain = 1

#waterTick = 0   #Used to count the number of times the flow input is triggered

#Assign channels to variables to keep track of them easier (these BCM pin numbers were listed in part 1 of the tutorial)
s1 = 13
s2 = 16
s3 = 19
s4 = 20
s5 = 26
s6 = 21



#Set GPIO pins to use BCM pin numbers
GPIO.setmode(GPIO.BCM)

#Set digital pin 24 to an input
GPIO.setup(24, GPIO.IN)

#Set solenoid driver pins to outputs:
GPIO.setup(s1, GPIO.OUT) #set Solenoid 1 output
GPIO.setup(s2, GPIO.OUT) #set Solenoid 2 output
GPIO.setup(s3, GPIO.OUT) #set Solenoid 3 output
GPIO.setup(s4, GPIO.OUT) #set Solenoid 4 output
GPIO.setup(s5, GPIO.OUT) #set Solenoid 5 output
GPIO.setup(s6, GPIO.OUT) #set Solenoid 6 output


#Event to detect flow (1 tick per revolution)
#GPIO.add_event_detect(24, GPIO.FALLING)
#def flowtrig(self):
#    global waterTick
#    waterTick += 1

#GPIO.add_event_callback(24, flowtrig)

#Email Variables
SMTP_SERVER = os.getenv("SMTP_SERVER") #Email Server (don't change!)
SMTP_PORT = os.getenv("SMTP_PORT") #Server Port (don't change!)
EMAIL_USERNAME = os.getenv("EMAIL_USERNAME")
EMAIL_PASSWORD = os.getenv("EMAIL_PASSWORD")
recipient = os.getenv("PHONE") #change this to your destination email account

#Start
#smStart = AnalogIn(ads, ADS.P0) #Read initial value from soil moisture sensor connected to A0

#smEnd = AnalogIn(ads, ADS.P0) #Read end value from soil moisture sensor connected to A0

#waterUsed = waterTick * 2.25 #convert our sensor count to millilitres
#waterUsed = waterUsed / 1000 #convert to liters

#create dictionary with database info
def get_data():
    con = sqlite3.connect("database.db")
    cursor = con.cursor()
    cursor.execute("Select daily_duration, zone, channel, name from watering")
    desc = cursor.description
    column_names = [col[0] for col in desc]

    zones = [dict(zip(column_names, row))
        for row in cursor.fetchall()]
    return zones

zone_list = get_data()

#Send Email
def send_email():
    #Email Subject
    subject = 'Sprinkler Update'

    #Email Content
    line0 = 'Run Completed \n'
    # line1 = 'Starting Moisture: ' + str(smStart.value) + '\n'
    # line2 = 'Ending Moisture: ' + str(smEnd.value) + '\n'
    # line3 = 'Water Consumed: ' + str(waterUsed) + '\n'
    line4 = 'Temperature: ' + str(ds18b20.get_temperature())

    emailText = line0 + line4

    #Email Headers
    headers = ["From: " + EMAIL_USERNAME, "Subject: " + subject, "To: " + recipient, "MIME-Version: 1.0", "Content-Type: text/html"]
    headers = "\r\n".join(headers)

    #Send Email
    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    session.ehlo()
    session.starttls()
    session.ehlo
    session.login(EMAIL_USERNAME, EMAIL_PASSWORD)
    session.sendmail(EMAIL_USERNAME, recipient, headers + "\r\n\r\n" + emailText)
    session.quit
    print("Email Sent")
#Done

#get time for automatic watering to start from DB, only one start time at the moment
def get_time():
    con = sqlite3.connect("database.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute("select time_of_day from water_time where zone=1")
    time = cur.fetchone();
    return time[0]

#watering to start according to time set, one zone at a time
def daily_watering():
    for zone in zone_list:
        print(zone['name'] +  " Watering Started")
        GPIO.output(zone['channel'], GPIO.HIGH) #turn zone on
        sleepTime = zone['daily_duration'] * 60 #multiply our run time (minutes) by 60 to get seconds
        time.sleep(sleepTime) # sleep for our duration with the solenoid open
        GPIO.output(zone['channel'], GPIO.LOW) #turn zone off
        print(zone['name'] + "Watering Finished")
    send_email()

#schedule daily task for watering according to specified time
scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(daily_watering, 'cron', hour=get_time(), id='start_watering', replace_existing=True)
scheduler.print_jobs()
# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())

#create form to for reuse
class ReusableForm(FlaskForm):
    duration = IntegerField('Duration', validators=[InputRequired()])
    submit = SubmitField('Submit')
    channel = HiddenField('Hidden')
    name = HiddenField('Name')

#populate variables to be used in an template
@app.context_processor
def inject_temp():
    temp = str(round(ds18b20.get_temperature(), 1)) #Get temperature from the temperature sensor
    moisture = AnalogIn(ads, ADS.P0).value #not used currently
    return dict(temp=temp, moisture=moisture)

@app.route('/', methods=['GET', 'POST'])
def main():
    print(request)
    form = ReusableForm(request.form)
    zones = get_data()
    print(form)
    now = datetime.datetime.now()
    timeString = now.strftime("%Y-%m-%d %H:%M")
    data = json.loads(json.dumps(zones), parse_int=str)
    templateData = {
        'time' : timeString,
        'zones' : data,
    }
    return render_template('main.html', **templateData, form=form)

#route for ad hoc watering based on inputted time
@app.route('/watering', methods=['GET', 'POST'])
def watering():
    print(request.form)
    name = request.form.get('name')
    channel = int(request.form.get('channel'))
    duration = float(request.form.get('duration'))
    def start_watering():
        print(name +  " Watering Started")
        GPIO.output(channel, GPIO.HIGH) #turn on zone
        sleepTime = duration * 60 #multiply our run time (minutes) by 60 to get seconds
        time.sleep(sleepTime) # sleep for our duration with the solenoid open
        GPIO.output(channel, GPIO.LOW) #turn zone off
        print(name + "Watering Finished")
    watering = threading.Thread(target=start_watering) #create background thread for watering
    watering.start()
    return (''), 204

#route for editing zones
@app.route('/timer', methods=['GET', 'POST'])
def timer():
    form = ReusableForm(request.form)
    rows = get_data()
    templateData = {
        'rows' : rows
    }
    con = sqlite3.connect("database.db")
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute("select time_of_day from water_time where zone=1")
    time = cur.fetchone();
    return render_template('timer.html', **templateData, form=form, time=time[0])

#route to change duration of watering
@app.route('/edit', methods=['POST'])
def edit():
    print("edit", request.form)
    con = sqlite3.connect("database.db")
    if request.method == 'POST':
        try:
            duration = request.form['duration']
            channel = request.form['channel']
            cur = con.cursor()
            cur.execute("UPDATE watering SET daily_duration=? WHERE channel=?", (duration, channel))
            con.commit()
            msg = "Record successfully added"
            print(msg)
        except:
           con.rollback()
           msg = "error in insert operation"
           print(msg)
        finally:
           con.close()
    return redirect(url_for('timer'))

#route to set the timer for daily watering
@app.route('/editTime', methods=['POST'])
def editTime():
    con = sqlite3.connect("database.db")
    if request.method == 'POST':
        print("post", request.form)
        duration = request.form['duration']
        try:
            print(duration)
            cur = con.cursor()
            cur.execute("UPDATE water_time SET time_of_day=? WHERE zone=1", (duration,))
            con.commit()
            msg = "Record successfully added"
            print(msg)
            scheduler.reschedule_job('start_watering', trigger='cron', hour=duration)
            scheduler.print_jobs()
        except:
           con.rollback()
           msg = "error in insert operation"
           print(msg)
        finally:
           con.close()
    return redirect(url_for('timer'))

#route to grab the current temperature
@app.route('/temperature', methods=['GET'])
def temperature():
    str(AnalogIn(ads, ADS.P0).value)
    temperature = str(round(ds18b20.get_temperature(), 1)) #Get temperature from the temperature sensor
    return temperature

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=True)
