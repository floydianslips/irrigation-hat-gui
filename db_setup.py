import sqlite3

#connect to DB and create tables
conn = sqlite3.connect('database.db')
print("Opened database successfully");

conn.execute('CREATE TABLE watering (name TEXT, zone INTEGER, daily_duration INTEGER, channel INTEGER)')
conn.execute('CREATE TABLE water_time (time_of_day INTEGER, zone INTEGER)')
print("Table created successfully");
conn.close()