# BC Robotics Irrigation Hat Gui

## Dependencies
raspiOS
sudo pip3 install --upgrade setuptools
pip3 install RPI.GPIO
pip3 install adafruit-blinka
sudo pip3 install adafruit-circuitpython-ads1x15
sudo apt-get install python3-w1thermsensor
sudo pip3 install flask-wtf
sudo pip3 install apscheduler
sudo pip3 install -U python-dotenv

### Install all using: sudo pip3 install -r requirement.txt

## This UI allows for Ad-Hoc watering and setting of watering timer

## Install:

Use this tutorial: https://bc-robotics.com/tutorials/raspberry-pi-irrigation-control-part-1-2/

1. Follow the steps until **step 12** of **part 2**. From here you can clone this repo.
2. Run `python3 db_setup.py`
3. Run `python3 db_insert.py` (this will create initial time and duration to be changed later if desired)
4. Run `sudo python3 run_irrigation.py` to start application and have fun!

**(Optional)** Start python script automatically at startup
5. Run `chmod +x launcher.sh` (This makes the launcher script executable)
6. `cd && mkdir logs` (Create a directory for error logs)
7. `sudo crontab -e` and add `@reboot sh /home/pi/irrigation-hat-gui/launcher.sh >/home/pi/logs/cronlog 2>&1` (Run launcher script and log errors upon startup)

## I don't have a waterflow sensor handy so I haven't incorporated that into this project. There are commented out lines in the main script that can easily be implemented. Moisture sensing is also set up but not implented.

![](images/irrigation-hat-1.png)
![](images/irrigation-hat-2.png)
![](images/irrigation-hat-3.png)


