import sqlite3

try:
    sqliteConnection = sqlite3.connect('database.db')
    cursor = sqliteConnection.cursor()
    print("Successfully Connected to SQLite")

    sqlite_insert_watering_1 = """INSERT INTO watering
                          (name, zone, daily_duration, channel)
                           VALUES
                          ('Lower Garden',2,40,16)"""

    sqlite_insert_watering_2 = """INSERT INTO watering
                          (name, zone, daily_duration, channel)
                           VALUES
                          ('Upper Garden',1,40,13)"""

    sqlite_insert_water_time = """INSERT INTO water_time
                          (zone, time_of_day)
                           VALUES
                          (1, 6)"""

    cursor.execute(sqlite_insert_watering_2)
    cursor.execute(sqlite_insert_watering_1)
    cursor.execute(sqlite_insert_water_time)
    sqliteConnection.commit()
    print("Record inserted successfully into irrigation table ")
    cursor.close()

except sqlite3.Error as error:
    print("Failed to insert data into sqlite table", error)
finally:
    if (sqliteConnection):
        sqliteConnection.close()
        print("The SQLite connection is closed")